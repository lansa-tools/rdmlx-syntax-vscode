# moves all relavant files into the artifacts folder
# note: uses relative paths to script
$root = Resolve-Path($PSScriptRoot + '\..\')
$artifacts = Join-Path $root 'artifacts'
$objects = @(
    'syntaxes',
    'language-configuration.json',
    'package.json'
)

New-Item -ItemType Directory -Path $artifacts -Force
Get-ChildItem -Path $artifacts | Remove-Item -Recurse -Force
$objects | foreach { Copy-Item -Path (Join-Path $root ('src\'+$_)) (Join-Path $artifacts $_) -Recurse }