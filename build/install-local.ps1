# need to run package-all.ps1 first

$root = Resolve-Path (Join-Path $PSScriptRoot '\..\artifacts\')
$packageRoot = "$env:USERPROFILE\.vscode\extensions\lansa-lang.syntax.dev\"

New-Item $packageRoot -ItemType Directory -Force
Get-ChildItem $root | Copy-Item -Destination $packageRoot -Recurse -Force