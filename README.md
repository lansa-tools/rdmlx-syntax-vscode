# lansa-language README

This is the VS Code syntax package for LANSA RDMLX. 

## Development

See the file `.\src\vsc-extension-quickstart.md` for a quick start. 
See https://code.visualstudio.com/api/language-extensions/syntax-highlight-guide for more information.

## Deployment / Installation

There are two powershell scripts under the `.\build\` folder.

Run `package-all.ps1` to get all necessary files gathered in the folder `.\artifacts\`. These are the files need to deploy the package. From anywhere run the script in PowerShell or CMD. Examples from when in root folder:

```ps
# powershell
.\build\package-all.ps1

# from command line
powershell -file ".\build\package-all.ps1"
```

After packaging up you can deploy locally by running `install-local.ps1`. This copies the files into the VS Code `extensions` folder in the user profile. You will need to reload VS Code after the files copy.

```ps
# powershell
.\build\package-all.ps1
.\build\install-local.ps1

# from command line
powershell -file ".\build\package-all.ps1"
powershell -file ".\build\install-local.ps1"
```

## Known Issues

No known issues at this time.

## Release Notes

Unreleased, still under development.

### 0.0.0

Finished hack-job of copying C# syntax grammar.